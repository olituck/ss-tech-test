const { fontFamily } = require("tailwindcss/defaultTheme");

module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: ["./components/**/*.{js,ts,jsx,tsx}", "./pages/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter var", ...fontFamily.sans],
      },
    },
    customForms: theme => ({
      default: {
        checkbox: {
          color: theme("colors.blue.500"),
          "&:indeterminate": {
            color: theme("colors.blue.500"),
            borderColor: theme("colors.blue.500"),
            backgroundColor: theme("colors.blue.500"),
            // icon:
            //   '<svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="4" y="8" width="12" height="4" rx="1" fill="white"/></svg>',
            backgroundImage:
              "url(\"data:image/svg+xml,%3Csvg viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Crect x='5' y='8' width='10' height='3' rx='1' fill='white'/%3E%3C/svg%3E \")",
          },
        },
        input: {
          backgroundColor: theme("colors.cool-gray.100"),
          color: theme("colors.cool-gray.700"),
          borderColor: theme("colors.transparent"),
          padding: `${theme("spacing.2")} ${theme("spacing.4")}`,
          lineHeight: theme("lineHeight.5"),
          fontSize: theme("fontSize.sm"),
          "&:focus": {
            backgroundColor: theme("colors.white"),
            borderColor: theme("colors.blue.500"),
          },
        },
        select: {
          backgroundColor: theme("colors.cool-gray.100"),
          color: theme("colors.cool-gray.700"),
          borderColor: theme("colors.transparent"),
          fontSize: theme("fontSize.sm"),
          "&:focus": {
            borderColor: theme("colors.blue.500"),
          },
        },
        textarea: {
          backgroundColor: theme("colors.cool-gray.100"),
          borderColor: theme("colors.transparent"),
          fontSize: theme("fontSize.sm"),
          "&:focus": {
            borderColor: theme("colors.blue.500"),
          },
        },
      },
    }),
  },
  variants: {},
  plugins: [
    require("@tailwindcss/ui")({
      layout: "sidebar",
    }),
  ],
};
